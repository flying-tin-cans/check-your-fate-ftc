import re

spawn_event = re.compile(r"^T:(?P<tik>\d+) AType:10 PLID:(?P<aircraft_id>\d+) PID:(?P<bot_id>\d+) BUL:(?P<cartridges>\d+) SH:(?P<shells>\d+) BOMB:(?P<bombs>\d+) RCT:(?P<rockets>\d+) \((?P<pos>.+)\) IDS:(?P<profile_id>[-\w]{36}) LOGIN:(?P<account_id>[-\w]{36}) NAME:(?P<name>.*) TYPE:(?P<aircraft_name>[\w\(\) .\-_/]+) COUNTRY:(?P<country_id>\d{1,3}) FORM:(?P<form>\d+) FIELD:(?P<airfield_id>\d+) INAIR:(?P<airstart>\d) PARENT:(?P<parent_id>[-\d]+) ISPL:(?P<is_player>1) ISTSTART:(?P<is_tracking_stat>\d+) PAYLOAD:(?P<payload_id>\d+) FUEL:(?P<fuel>\S{5,6}) SKIN:(?P<skin>[\S ]*) WM:(?P<weapon_mods_id>\d+)", re.MULTILINE)
takeoff_event = re.compile(r"^T:(?P<tik>\d+) AType:5 PID:(?P<aircraft_id>\d+) POS\((?P<pos>.+)\)$", re.MULTILINE)
landing_event = re.compile(r"^T:(?P<tik>\d+) AType:6 PID:(?P<aircraft_id>\d+) POS\((?P<pos>.+)\)$", re.MULTILINE)
damaged_event = re.compile(r"^T:(?P<tik>\d+) AType:2 DMG:(?P<damage>\S{5,6}) AID:(?P<attacker_id>[-\d]+) TID:(?P<target_id>[-\d]+) POS\((?P<pos>.+)\)$", re.MULTILINE)
destroyed_event = re.compile(r"^T:(?P<tik>\d+) AType:3 AID:(?P<attacker_id>[-\d]+) TID:(?P<target_id>[-\d]+) POS\((?P<pos>.+)\)$", re.MULTILINE)
disconnected_event = re.compile(r"^T:(?P<tik>\d+) AType:18 BOTID:(?P<bot_id>\d+) PARENTID:(?P<parent_id>[-\d]+) POS\((?P<pos>.+)\)$", re.MULTILINE)
sortie_end_event = re.compile(r"^T:(?P<tik>\d+) AType:4 PLID:(?P<aircraft_id>\d+) PID:(?P<bot_id>\d+) BUL:(?P<cartridges>\d+) SH:(?P<shells>\d+) BOMB:(?P<bombs>\d+) RCT:(?P<rockets>\d+) \((?P<pos>.+)\)$", re.MULTILINE)

def parse_event(log_line):
    event_data = {
        'type': 'other'
    }
    
    match = takeoff_event.match(log_line)
    if match:
        event_data = {
            'type': 'takeoff',
            'tik': int(match.group('tik')),
            'aid': match.group('aircraft_id'),
            'pos': match.group('pos')
        }

    match = landing_event.match(log_line)
    if match:
        event_data = {
            'type': 'landing',
            'tik': int(match.group('tik')),
            'aid': match.group('aircraft_id'),
            'pos': match.group('pos')
        }

    match = disconnected_event.match(log_line)
    if match:
        event_data = {
            'type': 'disconnected',
            'tik': int(match.group('tik')),
            'pid': match.group('bot_id'),
            'pos': match.group('pos')
        }

    match = sortie_end_event.match(log_line)
    if match:
        event_data = {
            'type': 'sortie_end',
            'tik': int(match.group('tik')),
            'pid': match.group('bot_id'),
            'pos': match.group('pos')
        }

    match = destroyed_event.match(log_line)
    if match:
        event_data = {
            'type': 'destroyed',
            'tik': int(match.group('tik')),
            'target_id': match.group('target_id'),
            'attacker_id': match.group('attacker_id'),
            'pos': match.group('pos')
        }

    match = damaged_event.match(log_line)
    if match:
        event_data = {
            'type': 'damaged',
            'tik': int(match.group('tik')),
            'target_id': match.group('target_id'),
            'attacker_id': match.group('attacker_id'),
            'pos': match.group('pos'),
            'damage': float(match.group('damage'))
        }
    
    return event_data
