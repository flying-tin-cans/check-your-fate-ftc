import json
from utils import cUtils
from pilot import cPilot
from sortie import cSortie
from il2_stats import cIl2_stats
from pam import cPam
from missionFrontline import cMissionFrontline

DEBUG_PRINT_ON = False
WARNING_PRINT_ON = True
INFO_PRINT_ON = True

FACTOR_TO_KM = float(1/1000) # Multiply to get km

class cCheckYourFate:
	def __init__(self):
		self.PAM_user = None
		self.PAM_password = None
		self.PAM_host = None
		self.PAM_database = None
		self.IL2stats_user = None
		self.IL2stats_password = None
		self.IL2stats_host = None
		self.IL2stats_database = None
		self.utils = cUtils(DEBUG_PRINT_ON, WARNING_PRINT_ON, INFO_PRINT_ON)
		self.il2_stats = None
		self.pam = None
		self.frontline = cMissionFrontline(self.utils)
		self.mission = None
		self.missionFile = None

	def executeForMission(self, mission, missionFile):
		self.utils.infoPrint("Executing check-your-fate for mission:", missionFile, "with il2_stats_db number:", mission)
		self.mission = mission
		self.missionFile = missionFile
		missionData = self.__getMissionData()
		missionFrontline = self.__getMissionFrontlineFromFile()
		missionFates = self.__calculateFate(missionData, missionFrontline["red"], missionFrontline["blue"])
		missionWrapUp = self.__calculateMissionWrapUp(missionFates)
		self.__disconnectFromDatabases()

	def __calculateMissionWrapUp(self, missionFates):
		missionWrapUp = []

		# Get base units
		query = ("SELECT id AS pam_baseunitid, name AS pam_baseunit FROM base_unit;")
		query_return = self.pam.query(query)
		for (pam_baseunitid, pam_baseunit) in query_return:
			missionWrapUpForUnit = {
				"baseUnitName": pam_baseunit,
				"sorties": 0,
				"pilotHealthy": 0,
				"pilotWounded": 0,
				"pilotDead": 0,
				"aircraftUnharmed": 0,
				"aircraftDamaged": 0,
				"aircraftDestroyed": 0
				}

			for fate in missionFates:
				if pam_baseunit == fate["pilotUnit"]:
					missionWrapUpForUnit["sorties"] = missionWrapUpForUnit["sorties"] + 1

					if "healthy" == fate["health"]:
						missionWrapUpForUnit["pilotHealthy"] = missionWrapUpForUnit["pilotHealthy"] + 1
					elif "wounded" == fate["health"]:
						missionWrapUpForUnit["pilotWounded"] = missionWrapUpForUnit["pilotWounded"] + 1
					elif "dead" == fate["health"]:
						missionWrapUpForUnit["pilotDead"] = missionWrapUpForUnit["pilotDead"] + 1
					else:
						self.utils.errorPrint("Check this fate for the pilot health:")
						self.utils.errorPrint(fate)

					if "unharmed" == fate["aircraftStatus"]:
						missionWrapUpForUnit["aircraftUnharmed"] = missionWrapUpForUnit["aircraftUnharmed"] + 1
					elif "damaged" == fate["aircraftStatus"]:
						missionWrapUpForUnit["aircraftDamaged"] = missionWrapUpForUnit["aircraftDamaged"] + 1
					elif "destroyed" == fate["aircraftStatus"]:
						missionWrapUpForUnit["aircraftDestroyed"] = missionWrapUpForUnit["aircraftDestroyed"] + 1
					else:
						self.utils.errorPrint("Check this fate for the aircraft status:")
						self.utils.errorPrint(fate)

			if 0 != missionWrapUpForUnit["sorties"]:
				missionWrapUp.append(missionWrapUpForUnit)

		filenname = "outputs/mission_wrap_up_for_mission_" + self.mission + ".json"
		self.utils.saveDataToJson(filenname, missionWrapUp)
		return missionWrapUp

	def __getMissionData(self):
		self.__setDbAccessInfo()
		self.__connectToDatabases()
		pilot_list_pam = self.__getPamPilots()
		pilot_list_il2stats = self.__getIl2StatsPilots()
		pilot_list_merged = self.__mergePamAndIl2StatsPilots(pilot_list_pam, pilot_list_il2stats)
		pilot_list_mission = self.__throwOutNonParticipants(pilot_list_merged, self.mission)
		pilot_list_mission = self.__addSorties(pilot_list_mission, self.mission)
		filenname = "outputs/output_for_mission_" + self.mission + ".json"
		missionData = self.__saveDataToJson(filenname, pilot_list_mission)

		return missionData

	def __getMissionFrontlineFromFile(self):
		missionFrontline = self.frontline.executeForMissionFile(self.mission, self.missionFile)
		return missionFrontline

	def __calculateFate(self, missionData, missionFrontlineRed, missionFrontlineBlue):
		missionFates = []
		for pilot in missionData:
			for sortie in pilot["sorties"]:
				posX = None
				posZ = None

				fate = {
					"pilotName": pilot["pam_callsign"],
					"pilotUnit": pilot["pam_baseunit"],
					"fate": None,
					"health": None,
					"aircraftStatus": None,
					"bailoutOrLandedOrDitchedPosition": None,
					"targetPosition": None,
					"capturedOrDeadPosition": None
				}

				if "not_takeoff" != sortie["il2stats_status"]:
					fate["health"] = sortie["il2stats_pilot_status"]
					fate["aircraftStatus"] = sortie["il2stats_aircraft_status"]

					# Check which frontline is friendly and which is not
					if 1 == sortie["il2stats_coalition"]:
						missionFrontlineFriendly = missionFrontlineRed
						missionFrontlineNotFriendly = missionFrontlineBlue
					elif 2 == sortie["il2stats_coalition"]:
						missionFrontlineFriendly = missionFrontlineBlue
						missionFrontlineNotFriendly = missionFrontlineRed
					else:
						self.utils.errorPrint("Sortie for", pilot["pam_callsign"], "has an error in the coalition, please check")

					# Check if pilot died:
					if (sortie["il2stats_pilot_status"] == "dead"):
						# Decide which position is the place the pilot died

						# Use the position end if it is not 0
						if ( (sortie["il2stats_position_end"]["x"] != "0.0") and
							 (sortie["il2stats_position_end"]["z"] != "0.0") ):
							posX = float(sortie["il2stats_position_end"]["x"])
							posZ = float(sortie["il2stats_position_end"]["z"])
						# Next good option is landed
						elif (sortie["il2stats_position_landed"] != None):
							posX = float(sortie["il2stats_position_landed"]["x"])
							posZ = float(sortie["il2stats_position_landed"]["z"])
						# Next good option is ditched
						elif (sortie["il2stats_position_ditched"] != None):
							posX = float(sortie["il2stats_position_ditched"]["x"])
							posZ = float(sortie["il2stats_position_ditched"]["z"])
						# Next good option is bailout
						elif (sortie["il2stats_position_bailout"] != None):
							posX = float(sortie["il2stats_position_bailout"]["x"])
							posZ = float(sortie["il2stats_position_bailout"]["z"])
						# Last option is crashed
						elif (sortie["il2stats_position_crashed"] != None):
							posX = float(sortie["il2stats_position_crashed"]["x"])
							posZ = float(sortie["il2stats_position_crashed"]["z"])
						else:
							self.utils.errorPrint("Sortie for", pilot["pam_callsign"], "which ended as dead has an error, please check")

						# Calculate point and distance for friendly lines
						shortesWaypointFriendly = self.utils.calculateDirectWayToLines(posX, posZ, missionFrontlineFriendly)

						# Calculate point and distance for not friendly lines
						shortesWaypointNotFriendly = self.utils.calculateDirectWayToLines(posX, posZ, missionFrontlineNotFriendly)

						# Check which side of the frontline the pilot died
						if shortesWaypointFriendly["Distance"] > shortesWaypointNotFriendly["Distance"]:
							fate["fate"] = "Died behind enemy lines"
						else:
							fate["fate"] = "Died within friendly lines"

						# Write position of death
						fate["capturedOrDeadPosition"] = {"XPos": posX,"ZPos": posZ}

					# Check if pilot bailed out (and did not die):
					elif (sortie["ilstats_bailout"] == True) and (sortie["il2stats_pilot_status"] != "dead"):
						# Check if the landed position is available and use it:
						if None != sortie["il2stats_position_landed"]:
							posX = float(sortie["il2stats_position_landed"]["x"])
							posZ = float(sortie["il2stats_position_landed"]["z"])
						# If not, use the bailout position
						else:
							posX = float(sortie["il2stats_position_bailout"]["x"])
							posZ = float(sortie["il2stats_position_bailout"]["z"])

						fate["bailoutOrLandedOrDitchedPosition"] = {"XPos": posX,"ZPos": posZ}

						# Calculate point and distance for friendly lines
						shortesWaypointFriendly = self.utils.calculateDirectWayToLines(posX, posZ, missionFrontlineFriendly)

						# Calculate point and distance for not friendly lines
						shortesWaypointNotFriendly = self.utils.calculateDirectWayToLines(posX, posZ, missionFrontlineNotFriendly)

						if shortesWaypointFriendly["Distance"] > shortesWaypointNotFriendly["Distance"]:
							# Landed behind enemy lines
							fate["targetPosition"] = shortesWaypointFriendly

							distanceToCoverInKm = shortesWaypointFriendly["Distance"] * FACTOR_TO_KM
							escape = self.utils.calculateEscape(distanceToCoverInKm)

							# If captured en-route, calculate the point how far the pilot came
							if True == escape["Captured-en-route"]:
								distanceCoveredInKm = escape["Distance-covered-if-captured"]
								capturedPoint = self.utils.caclulcatePointOfCapture(
									distanceCoveredInKm / FACTOR_TO_KM,
									fate["bailoutOrLandedOrDitchedPosition"],
									fate["targetPosition"]
									)
								fate["capturedOrDeadPosition"] = capturedPoint
								escape["Captured-en-route"]

								fate["fate"] = "Captured from enemy after bailout"
							else:
								fate["fate"] = "Escpaed from enemy lines after bailout"

						else:
							# Landed behind friendly lines
							fate["fate"] = "Bailed out within friendly lines"

					# Check if pilot landed and if yes if that was within friendly lines
					elif "landed" == sortie["il2stats_status"]:
						posX = float(sortie["il2stats_position_landed"]["x"])
						posZ = float(sortie["il2stats_position_landed"]["z"])

						fate["bailoutOrLandedOrDitchedPosition"] = {"XPos": posX,"ZPos": posZ}

						# Calculate point and distance for friendly lines
						shortesWaypointFriendly = self.utils.calculateDirectWayToLines(posX, posZ, missionFrontlineFriendly)

						# Calculate point and distance for not friendly lines
						shortesWaypointNotFriendly = self.utils.calculateDirectWayToLines(posX, posZ, missionFrontlineNotFriendly)

						if shortesWaypointFriendly["Distance"] > shortesWaypointNotFriendly["Distance"]:
							fate["fate"] = "Landed behind enemy lines"
						else:
							fate["fate"] = "Landed within friendly lines"

					elif "in_flight" == sortie["il2stats_status"]:
						fate["fate"] = "Disconnected"

					elif "shotdown" == sortie["il2stats_status"]:
						# Check if we want ditched or crashed as position
						if None != sortie["il2stats_position_ditched"]:
							posX = float(sortie["il2stats_position_ditched"]["x"])
							posZ = float(sortie["il2stats_position_ditched"]["z"])
						elif None != sortie["il2stats_position_crashed"]:
							posX = float(sortie["il2stats_position_crashed"]["x"])
							posZ = float(sortie["il2stats_position_crashed"]["z"])
						else:
							self.utils.errorPrint("Shotdown position error, check this sortie:")
							self.utils.errorPrint(sortie)

						fate["bailoutOrLandedOrDitchedPosition"] = {"XPos": posX,"ZPos": posZ}

						# Calculate point and distance for friendly lines
						shortesWaypointFriendly = self.utils.calculateDirectWayToLines(posX, posZ, missionFrontlineFriendly)

						# Calculate point and distance for not friendly lines
						shortesWaypointNotFriendly = self.utils.calculateDirectWayToLines(posX, posZ, missionFrontlineNotFriendly)

						if shortesWaypointFriendly["Distance"] > shortesWaypointNotFriendly["Distance"]:
							fate["fate"] = "Ditched behind enemy lines"
						else:
							fate["fate"] = "Ditched within friendly lines"

					else:
						self.utils.errorPrint("Check sortie for", pilot["pam_callsign"])
						self.utils.errorPrint(sortie)

					missionFates.append(fate)

		filenname = "outputs/fates_for_mission_" + self.mission + ".json"
		self.utils.saveDataToJson(filenname, missionFates)

		return missionFates

	def __setDbAccessInfo(self):
		try:
			with open("inputs/secret.json", "r") as json_file:
				secrets = json.load(json_file)

			self.PAM_user = secrets["PAM_user"]
			self.PAM_password = secrets["PAM_password"]
			self.PAM_host = secrets["PAM_host"]
			self.PAM_database = secrets["PAM_database"]
			self.IL2stats_user = secrets["IL2stats_user"]
			self.IL2stats_password = secrets["IL2stats_password"]
			self.IL2stats_host = secrets["IL2stats_host"]
			self.IL2stats_database = secrets["IL2stats_database"]

			self.utils.infoPrint("Got connection information from file")
		except FileNotFoundError as err:
			self.utils.errorPrint(err)
			self.utils.fatalError("Config read-out failed")

	def __connectToDatabases(self):
		self.il2_stats = cIl2_stats(self.IL2stats_host, self.IL2stats_user, self.IL2stats_password, self.IL2stats_database, self.utils)
		self.il2_stats.createConnection()
		self.pam = cPam(self.PAM_host, self.PAM_user, self.PAM_password, self.PAM_database, self.utils)
		self.pam.createConnection()

	def __disconnectFromDatabases(self):
		self.il2_stats.closeConnection()
		self.pam.closeConnection()

	def __saveDataToJson(self, filenname, pilot_list_mission):
		dictList = []

		for pilot in pilot_list_mission:
			dictList.append(pilot.toDictionary())

		self.utils.saveDataToJson(filenname, dictList)

		return dictList

	def __getPamPilots(self):
		# Get pilot info
		query = ("SELECT member.id AS pam_id, member.callsign AS pam_callsign, member.boxid AS pam_boxid, current_unit_members.base_unit_id AS pam_baseunitid FROM member INNER JOIN current_unit_members ON member.id = current_unit_members.id;")
		query_return = self.pam.query(query)
		pilot_list = []
		for (pam_id, pam_callsign, pam_boxid, pam_baseunitid) in query_return:
			pilot_list.append(cPilot(pam_id, pam_callsign, pam_boxid, pam_baseunitid))

		# Get base unit name
		query = ("SELECT id AS pam_baseunitid, name AS pam_baseunit FROM base_unit;")
		query_return = self.pam.query(query)
		for (pam_baseunitid, pam_baseunit) in query_return:
			for pilot in pilot_list:
				if pam_baseunitid == pilot.pam_baseunitid:
					pilot.pam_baseunit = pam_baseunit

		return pilot_list

	def __getIl2StatsPilots(self):
		query_results = []
		query = ("SELECT id AS il2stats_id, uuid AS il2stats_boxid, nickname AS il2stats_nickname FROM profiles;")
		query_return = self.il2_stats.query(query)
		for (il2stats_id, il2stats_boxid, il2stats_nickname) in query_return:
			query_results.append({"il2stats_id": il2stats_id, "il2stats_boxid": il2stats_boxid, "il2stats_nickname": il2stats_nickname})
		return query_results

	def __mergePamAndIl2StatsPilots(self, pilot_list_pam, pilot_list_il2stats):
		for pilot in pilot_list_pam:
			for x in pilot_list_il2stats:
				if pilot.pam_boxid == x["il2stats_boxid"]:
					pilot.il2stats_id = x["il2stats_id"]
					pilot.il2stats_boxid = x["il2stats_boxid"]
					pilot.il2stats_nickname = x["il2stats_nickname"]
					self.utils.debugPrint("Found pilot:",pilot.pam_callsign,"with il2_stats_db ID:", pilot.il2stats_id)
					break
		return pilot_list_pam

	def __throwOutNonParticipants(self, pilot_list_merged, mission):
		query_results = []

		# Sort out the pilots who did not flew this mission:
		query = ("SELECT id AS il2stats_sortie_id, profile_id AS il2stats_id, status AS il2stats_status, aircraft_status AS il2stats_aircraft_status, bot_status AS il2stats_pilot_status, is_bailout AS ilstats_bailout, coalition AS il2stats_coalition FROM sorties WHERE mission_id = " + mission + ";")

		query_return = self.il2_stats.query(query)

		pilot_list_mission = []

		# Search for the pilots in the sorties
		for (il2stats_sortie_id, il2stats_id, il2stats_status, il2stats_aircraft_status, il2stats_pilot_status, ilstats_bailout, il2stats_coalition) in query_return:
			query_results.append({
				"il2stats_sortie_id": il2stats_sortie_id,
				"il2stats_id": il2stats_id,
				"il2stats_status": il2stats_status,
				"il2stats_aircraft_status": il2stats_aircraft_status,
				"il2stats_pilot_status": il2stats_pilot_status,
				"ilstats_bailout": ilstats_bailout,
				"il2stats_coalition": il2stats_coalition})

		for pilot in pilot_list_merged:
			for x in query_results:
				if pilot.il2stats_id == x["il2stats_id"]:
					pilot.sorties.append(cSortie(x["il2stats_sortie_id"], x["il2stats_coalition"], x["il2stats_status"], x["il2stats_aircraft_status"], x["il2stats_pilot_status"], x["ilstats_bailout"]))
					if pilot not in pilot_list_mission:
						self.utils.debugPrint("First sortie found for", pilot.pam_callsign)
						pilot_list_mission.append(pilot)
					self.utils.debugPrint("Found sortie for:", pilot.pam_callsign, "with il2stats_id", pilot.il2stats_id, "with il2_stats_db sortie ID", x["il2stats_sortie_id"])

		return pilot_list_mission

	def __addSorties(self, pilot_list_mission, mission):
		query_results = []

		# Add information to the sorties
		query_results.clear()
		query = ("SELECT act_sortie_id AS il2stats_sortie_id, type AS il2stats_type, extra_data AS il2stats_position, id AS il2stats_log_entrie_id FROM log_entries WHERE mission_id = " + mission + " AND cact_object_id IS NULL;")

		query_return = self.il2_stats.query(query)

		for (il2stats_sortie_id, il2stats_type, il2stats_position, il2stats_log_entrie_id) in query_return:
			query_results.append({"il2stats_sortie_id": il2stats_sortie_id, "il2stats_type": il2stats_type, "il2stats_position": il2stats_position, "il2stats_log_entrie_id": il2stats_log_entrie_id})

		for pilot in pilot_list_mission:
			self.utils.debugPrint("Checking for", pilot.pam_callsign)
			for sortie in pilot.sorties:
				self.utils.debugPrint("Checking sortie", sortie.il2stats_sortie_id, "for", pilot.pam_callsign)
				for x in query_results:
					if sortie.il2stats_sortie_id == x["il2stats_sortie_id"]:
						self.utils.debugPrint("Found sortie match", sortie.il2stats_sortie_id)
						if "bailout" == x["il2stats_type"]:
							sortie.il2stats_position_bailout = self.utils.translatePosData(x["il2stats_position"])
							self.utils.debugPrint("Pilot", pilot.pam_callsign,"bailed out at", x["il2stats_position"], "in il2_stats_db sortie ID", sortie.il2stats_sortie_id, "with il2stats_log_entrie_id", x["il2stats_log_entrie_id"])
						elif "crashed" == x["il2stats_type"]:
							sortie.il2stats_position_crashed = self.utils.translatePosData(x["il2stats_position"])
							self.utils.debugPrint("Aircraft of Pilot", pilot.pam_callsign,"crashed at", x["il2stats_position"], "in il2_stats_db sortie ID", sortie.il2stats_sortie_id, "with il2stats_log_entrie_id", x["il2stats_log_entrie_id"])
						elif "ditched" == x["il2stats_type"]:
							sortie.il2stats_position_ditched = self.utils.translatePosData(x["il2stats_position"])
							self.utils.debugPrint("Pilot", pilot.pam_callsign,"ditched at", x["il2stats_position"], "in il2_stats_db sortie ID", sortie.il2stats_sortie_id, "with il2stats_log_entrie_id", x["il2stats_log_entrie_id"])
						elif "end" == x["il2stats_type"]:
							sortie.il2stats_position_end = self.utils.translatePosData(x["il2stats_position"])
							self.utils.debugPrint("Pilot", pilot.pam_callsign,"ended sortie at", x["il2stats_position"], "in il2_stats_db sortie ID", sortie.il2stats_sortie_id, "with il2stats_log_entrie_id", x["il2stats_log_entrie_id"])
						elif "landed" == x["il2stats_type"]:
							sortie.il2stats_position_landed = self.utils.translatePosData(x["il2stats_position"])
							self.utils.debugPrint("Pilot", pilot.pam_callsign,"landed at", x["il2stats_position"], "in il2_stats_db sortie ID", sortie.il2stats_sortie_id, "with il2stats_log_entrie_id", x["il2stats_log_entrie_id"])
						elif "respawn" == x["il2stats_type"]:
							sortie.il2stats_position_respawn = self.utils.translatePosData(x["il2stats_position"])
							self.utils.debugPrint("Pilot", pilot.pam_callsign,"(re)spawned at", x["il2stats_position"], "in il2_stats_db sortie ID", sortie.il2stats_sortie_id, "with il2stats_log_entrie_id", x["il2stats_log_entrie_id"])
						elif "takeoff" == x["il2stats_type"]:
							sortie.il2stats_position_takeoff = self.utils.translatePosData(x["il2stats_position"])
							self.utils.debugPrint("Pilot", pilot.pam_callsign,"took off at", x["il2stats_position"], "in il2_stats_db sortie ID", sortie.il2stats_sortie_id, "with il2stats_log_entrie_id", x["il2stats_log_entrie_id"])
						else:
							self.utils.warningPrint("Something went wrong for", pilot.pam_callsign, "in sortie", sortie.il2stats_sortie_id, "with il2stats_log_entrie_id", x["il2stats_log_entrie_id"])

		return pilot_list_mission
