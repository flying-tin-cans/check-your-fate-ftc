import mysql.connector
from mysql.connector import errorcode
from utils import * 

class cPam:
	def __init__(self, host, user, password, database, utils):
		self.host = host
		self.user = user
		self.password = password
		self.database = database
		self.pam = None
		self.cursor = None
		self.utils = utils

	def createConnection(self):
		try:
			self.pam = mysql.connector.connect(user=self.user, password=self.password,
                              host=self.host,
                              database=self.database)
			self.cursor = self.pam.cursor()
			self.utils.infoPrint("PAM connection created")
		except mysql.connector.Error as err:
			if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
				self.utils.errorPrint("Something is wrong with your user name or password")
			elif err.errno == errorcode.ER_BAD_DB_ERROR:
				self.utils.errorPrint("Database does not exist")
			else:
				self.utils.errorPrint(err)
			self.utils.fatalError("PAM connection creation failed")

	def closeConnection(self):
		self.cursor.close()
		self.pam.close()
		self.utils.infoPrint("Closed PAM connection")

	def query(self, query):
		try:
			self.cursor.execute(query)
			return self.cursor
		except mysql.connector.Error as err:
			self.utils.errorPrint(err)
			self.utils.fatalError("PAM query failed")
