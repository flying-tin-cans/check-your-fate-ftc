import json
from shapely.geometry import LineString as shLs
from shapely.geometry import Point as shPt
from utils import cUtils

ImageSizeX = 0
ImageSizeZ = 0

class cFrontlinePoints:
	def __init__(self, XPos, YPos, ZPos, Index, Target):
		self.XPos = XPos
		self.YPos = YPos
		self.ZPos = ZPos
		self.Index = Index 
		self.Target = Target
		self.pointerNext = None

	def __getitem__(self, key):
		return self

	def toString(self):
		return(
			"Index:", self.Index,
			"Target:", self.Target,
			"XPos:", self.XPos,
			"YPos:", self.YPos,
			"ZPos:", self.ZPos,
			"pointerNext:", self.pointerNext
			)

	def toDict(self):
		return(
			{
				"XPos": self.XPos,
				"YPos": self.YPos,
				"ZPos": self.ZPos
			}
		)

class cMissionFrontline:
	def __init__(self, cUtils):
		self.frontlinePointsRed = []
		self.frontlinePointsBlue = []
		self.utils = cUtils
		self.mission = None
		self.filename  = None

	def executeForMissionFile(self, mission, filenname):
		# Clear list
		self.frontlinePointsRed = []
		self.frontlinePointsBlue = []

		self.mission = mission
		self.filename  = filenname
		
		frontline = {
			"red": self.__getFrontline("red"),
			"blue": self.__getFrontline("blue")
			}
		
		return frontline

	def __getFrontline(self, coalition):
		with open(self.filename, 'r', encoding='utf-8') as missionfile:
			data=missionfile.read()

		# Get start of the frontline data
		if "red" == coalition:
			startFrontLines = data.find('Group\n{\n  Name = \"Front Lines - Allies\";')
			frontlinePoints = self.frontlinePointsRed
			filennameJson = "outputs/frontline_red_for_mission_" + self.mission + ".json";
		if "blue" == coalition:
			startFrontLines = data.find('Group\n{\n  Name = \"Front Lines - Axis\";')
			frontlinePoints = self.frontlinePointsBlue
			filennameJson = "outputs/frontline_blue_for_mission_" + self.mission + ".json";

		# Delete the string stuff before it
		data = data[startFrontLines:]
		# Find the start of the next Group
		endFrontLines = data.find('\nGroup')
		# Delete the rest of the string
		data = data[:endFrontLines]
		# Get rid of the spaces
		data = data.replace(' ', '')
		# Get rid of the semicolons
		data = data.replace(';', '')
		# Get rid of the brackets
		data = data.replace('[', '')
		data = data.replace(']', '')
		# Convert every line into a list entry
		data = list(data.split('\n'))

		frontlinePoints = []

		for index, content in enumerate(data, start=0):
			if content.startswith('MCU_Icon'):
				XPos = data[index+5].replace('XPos=', '')
				YPos = data[index+6].replace('YPos=', '')
				ZPos = data[index+7].replace('ZPos=', '')
				Index = data[index+2].replace('Index=', '')
				Target = data[index+3].replace('Targets=', '')

				Index = int(Index)
				# Target can be empty (this means this is the last point of the frontline)
				if Target != '':
					Target = int(Target)

				frontlinePoints.append(cFrontlinePoints(XPos, YPos, ZPos, Index, Target))

		# Fill in the targets
		for frontlinePoint in frontlinePoints:
			if (frontlinePoint.Target != '') and (frontlinePoint.pointerNext == None):
				frontlinePoint.pointerNext = self.__findTarget(frontlinePoints, frontlinePoint.Target)
		
		# Sort the frontline
		frontlinePoints = self.__createSortedList(frontlinePoints)
 
		# Make data JSON serializable
		frontlinePointsForJson = []
		for frontlinePoint in frontlinePoints:
			frontlinePointsForJson.append(frontlinePoint.toDict())

		self.utils.saveDataToJson(filennameJson, frontlinePointsForJson)

		return frontlinePointsForJson

	def __findTarget(self, givenList, target):
		for listItem in givenList:
			if listItem.Index == target:
				return listItem

	def __createSortedList(self, unsortedList):
		sortedList = []
		item = unsortedList[0]
		while item.pointerNext != None:
			sortedList.append(item)
			item = item.pointerNext
		return sortedList

def test1():
	utils = cUtils(True, True, True)
	frontline = []
	frontline.append(cFrontlinePoints(0,0,0,0,0))
	frontline.append(cFrontlinePoints(2,0,2,0,0))
	pointX = 0
	pointZ = 2

	pointForCrossing = utils.calculateDirectWayToFriendlyLines(pointX, pointZ, frontline)
	utils.infoPrint("pointForCrossing expected 1,1")
	utils.infoPrint("pointForCrossing calculated =", pointForCrossing)

def test2():
	utils = cUtils(True, True, True)

	missionFrontLine = cMissionFrontline(utils)
	missionFrontLine.executeForMissionFile("9", "FTC_-_Frontline-Test-Coalitions.Mission")

if __name__ == "__main__":
	#test1()
	test2()
