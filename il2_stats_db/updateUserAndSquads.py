import json
import os
from datetime import date
from utils import *
from il2_stats import *
from pam import *

DEBUG_PRINT_ON = False
WARNING_PRINT_ON = True
INFO_PRINT_ON = True

class cUpdateUserAndSquads:
	def __init__(self):
		self.PAM_user = None
		self.PAM_password = None
		self.PAM_host = None
		self.PAM_database = None
		self.IL2stats_user = None
		self.IL2stats_password = None
		self.IL2stats_host = None
		self.IL2stats_database = None
		self.utils = cUtils(DEBUG_PRINT_ON, WARNING_PRINT_ON, INFO_PRINT_ON)
		self.il2_stats = None
		self.pam = None

	def execute(self):
		self.__setDbAccessInfo()
		self.__connectToDatabases()
		pilot_list_pam = self.__getPamPilots()
		pilot_list_il2stats = self.__getIl2StatsPilots()
		pilots_not_linked = self.__getPilotsNotLinked(pilot_list_pam, pilot_list_il2stats)
		today = date.today().strftime("%Y%m%d")
		self.__saveDataToJson(today + "-pilots_not_linked", pilots_not_linked)
		pilot_list_merged = self.__mergePamAndIl2StatsPilots(pilot_list_pam, pilot_list_il2stats)
		self.__updateIl2StatsUsers(pilot_list_merged)
		self.__linkIl2StatsProfilesToUsers(pilot_list_merged)
		#self.__linkIl2StatsProfilesToSquads()
		self.__disconnectFromDatabases()

	def __setDbAccessInfo(self):
		try:
			f = open("secret.cfg", "r")
			self.PAM_user = f.readline().rstrip()
			self.PAM_password = f.readline().rstrip()
			self.PAM_host = f.readline().rstrip()
			self.PAM_database = f.readline().rstrip()
			self.IL2stats_user = f.readline().rstrip()
			self.IL2stats_password = f.readline().rstrip()
			self.IL2stats_host = f.readline().rstrip()
			self.IL2stats_database = f.readline().rstrip()
			f.close()
			self.utils.infoPrint("Got connection information from file")
		except FileNotFoundError as err:
			self.utils.errorPrint(err)
			self.utils.fatalError("Config read-out failed")

	def __connectToDatabases(self):
		self.il2_stats = cIl2_stats(self.IL2stats_host, self.IL2stats_user, self.IL2stats_password, self.IL2stats_database, self.utils)
		self.il2_stats.createConnection()
		self.pam = cPam(self.PAM_host, self.PAM_user, self.PAM_password, self.PAM_database, self.utils)
		self.pam.createConnection()

	def __disconnectFromDatabases(self):
		self.il2_stats.closeConnection()
		self.pam.closeConnection()

	def __getPamPilots(self):
		query_results = []
		query = ("SELECT id AS pam_id, callsign AS pam_callsign, boxid AS pam_boxid FROM member;")
		query_return = self.pam.query(query)
		query_results = []
		for (pam_id, pam_callsign, pam_boxid) in query_return:
			query_results.append({"pam_id": pam_id, "pam_callsign": pam_callsign, "pam_boxid": pam_boxid, "is_in_il2stats": False})
		return query_results

	def __getIl2StatsPilots(self):
		query_results = []
		query = ("SELECT id AS il2stats_id, uuid AS il2stats_boxid, nickname AS il2stats_nickname, user_id AS il2stats_userid, squad_id AS il2stats_squadid FROM profiles;")
		query_return = self.il2_stats.query(query)
		for (il2stats_id, il2stats_boxid, il2stats_nickname, il2stats_userid, il2stats_squadid) in query_return:
			query_results.append({"il2stats_id": il2stats_id, "il2stats_boxid": il2stats_boxid, "il2stats_nickname": il2stats_nickname, "il2stats_userid": il2stats_userid, "il2stats_squadid": il2stats_squadid,  "is_in_pam": False})
		return query_results

	def __getIl2StatsUsers(self):
		query_results = []
		query = ("SELECT id AS il2stats_userid, username AS il2stats_username FROM users;")
		query_return = self.il2_stats.query(query)
		for (il2stats_userid, il2stats_username) in query_return:
			query_results.append({"il2stats_userid": il2stats_userid, "il2stats_username": il2stats_username})
		return query_results

	def __getPilotsNotLinked(self, pilot_list_pam, pilot_list_il2stats):
		for pam_pilot in pilot_list_pam:
			for pilot_il2stats in pilot_list_il2stats:
				if pam_pilot["pam_boxid"] == pilot_il2stats["il2stats_boxid"]:
					pam_pilot["is_in_il2stats"] = True
					pilot_il2stats["is_in_pam"] = True

		pilots_not_linked = []

		for pam_pilot in pilot_list_pam:
			if False == pam_pilot["is_in_il2stats"]:
				pilots_not_linked.append(pam_pilot)

		for pilot_il2stats in pilot_list_il2stats:
			if False == pilot_il2stats["is_in_pam"]:
				pilots_not_linked.append(pilot_il2stats)

		return pilots_not_linked

	# Merge the pam and il2stats pilots lists on base of the box-id
	def __mergePamAndIl2StatsPilots(self, pilot_list_pam, pilot_list_il2stats):
		for pam_pilot in pilot_list_pam:
			for il2stats_pilot in pilot_list_il2stats:
				if pam_pilot["pam_boxid"] == il2stats_pilot["il2stats_boxid"]:
					pam_pilot["il2stats_id"] = il2stats_pilot["il2stats_id"]
					pam_pilot["il2stats_boxid"] = il2stats_pilot["il2stats_boxid"]
					pam_pilot["il2stats_nickname"] = il2stats_pilot["il2stats_nickname"]
					break
		return pilot_list_pam

	# Checks if the PAM pilot is in the il2stats DB as an "user" and if not adds him there
	# but only if we have a Box-Id from him
	def __updateIl2StatsUsers(self, pilot_list_merged):
		query_results = []
		for pilot in pilot_list_merged:
			if None != pilot["pam_boxid"]:
				querystring = "SELECT id as il2stats_userid FROM users WHERE id = " + str(pilot["pam_id"]) + ";"
				query = (querystring)
				query_return = self.il2_stats.query(query)

				if None == query_return.fetchone():
					# PW is "PhoneixFromTheAshes!"
					querystring = "INSERT INTO users (id, password, is_superuser, username, email, is_staff, is_active, date_joined, tz) VALUES (" + \
						"'" + str(pilot["pam_id"]) + \
						"', 'argon2$argon2i$v=19$m=512,t=2,p=2$MFpEaUhrRldtd3pD$w1cx6FxyILFnKp2wK8s1Dg', FALSE, " + \
						"'" + pilot["il2stats_nickname"] + "', " + \
						"'ftc.flyingtincans+" + pilot["pam_callsign"] + "@gmail.com', " + \
						"FALSE, TRUE, '2000-01-01', 'UTC');"
					
					print(querystring)
					query = (querystring) 
					query_return = self.il2_stats.query(query)

	def __linkIl2StatsProfilesToUsers(self, pilot_list_merged):
		query_results = []
		for pilot in pilot_list_merged:
			if None != pilot["pam_boxid"]:
				querystring = "SELECT nickname AS il2stats_nickname FROM profiles WHERE uuid='" + pilot["pam_boxid"] + "' AND user_id IS NULL;"
				query = (querystring)
				query_return = self.il2_stats.query(query)
				if None != query_return.fetchone():
					querystring= "UPDATE profiles SET user_id = '" + str(pilot["pam_id"]) + "' WHERE uuid = '" + pilot["pam_boxid"] + "'"
					query = (querystring)
					query_return = self.il2_stats.query(query)

	def __linkIl2StatsProfilesToSquads(self):
		query_results = []
		querystring = "SELECT member_id as pam_id, base_unit_id as pam_unit_id FROM current_unit_members"
		query = (querystring)
		query_return = self.pam.query(query)
		for (pam_id, base_unit_id) in query_return:
			query_results.append({"pam_id": pam_id, "base_unit_id": base_unit_id})

		for pilot in query_results:
			querystring = "UPDATE profiles SET squad_id = " + str(pilot["base_unit_id"]) + " WHERE user_id=" + str(pilot["pam_id"]) + ";"
			query = (querystring)
			query_return = self.il2_stats.query(query)

		for pilot in query_results:
			querystring = "SELECT member_id AS il2stats_member_id FROM squads_members WHERE member_id = " + str(pilot["pam_id"]) + ";"
			query = (querystring)
			query_return = self.il2_stats.query(query)

			if None == query_return.fetchone():
				querystring = "INSERT INTO squads_members (member_id, is_admin, date_joined, squad_id) VALUES (" + \
					"'" + str(pilot["pam_id"]) + \
					"', FALSE, '2000-01-01', '" +  \
					str(pilot["base_unit_id"]) + "');"
			else:
				querystring = "UPDATE squads_members SET squad_id = " + str(pilot["base_unit_id"]) + " WHERE member_id = " + str(pilot["pam_id"]) + ";"

			query = (querystring)
			query_return = self.il2_stats.query(query)


	def __saveDataToJson(self, filenname, inputlist):
		filnameJson = filenname + ".json"

		if os.path.exists(filnameJson):
			os.remove(filnameJson)

		with open(filnameJson, 'w') as outfile:
			json.dump(inputlist, outfile, indent=4)

def main():
	updateUserAndSquads = cUpdateUserAndSquads()
	updateUserAndSquads.execute()


if __name__ == "__main__":
	main()
