import psycopg2
from utils import * 

class cIl2_stats:
	def __init__(self, host, user, password, database, utils):
		self.host = host
		self.user = user
		self.password = password
		self.database = database
		self.il2stats = None
		self.cursor = None
		self.utils = utils

	def createConnection(self):
		try:
			self.il2stats = psycopg2.connect(
			    host=self.host,
			    database=self.database,
			    user=self.user,
			    password=self.password)
			self.il2stats.autocommit = True
			self.cursor = self.il2stats.cursor()
			self.utils.infoPrint("il2_stats_db connection created")
		except psycopg2.OperationalError as err:
			self.utils.errorPrint(err)
			self.utils.fatalError("il2_stats_db connection creation failed")

	def closeConnection(self):
		self.cursor.close()
		self.il2stats.close()
		self.utils.infoPrint("Closed il2_stats_db connection")

	def query(self, query):
		try:
			self.cursor.execute(query)
			return self.cursor
		except psycopg2.InterfaceError as err:
			self.utils.errorPrint(err)
			self.utils.fatalError("il2_stats query failed")
