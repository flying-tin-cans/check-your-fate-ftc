class cPilot:
	def __init__ (self, pam_id, pam_callsign, pam_boxid, pam_baseunitid):
		self.pam_id = pam_id
		self.pam_callsign = pam_callsign
		self.pam_baseunitid = pam_baseunitid
		self.pam_baseunit = None
		self.pam_boxid = pam_boxid
		self.il2stats_id = None
		self.il2stats_boxid = None
		self.il2stats_nickname = None
		self.sorties = []

	def toDictionary(self):
		pilot_dict = {
		"pam_id": self.pam_id,
		"pam_callsign": self.pam_callsign,
		"pam_baseunitid": self.pam_baseunitid,
		"pam_baseunit": self.pam_baseunit,
		"boxid": self.pam_boxid,
		"il2stats_id": self.il2stats_id,
		"il2stats_nickname": self.il2stats_nickname,
		"sorties": []
		}

		for sortie in self.sorties:
			pilot_dict["sorties"].append(sortie.toDictionary())

		return pilot_dict
