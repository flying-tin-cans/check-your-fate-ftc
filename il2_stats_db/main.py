import json
from checkYourFate import cCheckYourFate

def main():
	checkYourFate = cCheckYourFate()

	with open("inputs/missions.json") as json_file:
		missions = json.load(json_file)

	for mission in missions:
		checkYourFate.executeForMission(str(mission["il2_stats_mission"]), "inputs/" + mission["missionFileName"])

if __name__ == "__main__":
	main()
