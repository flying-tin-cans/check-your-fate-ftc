# Check Your Fate
Overview about the components used for FTCs "Check-Your-Fate" feature:
```mermaid
sequenceDiagram
PAM ->> Check-Your-Fate: Get active FTC pilots
IL2_stats_db ->> Check-Your-Fate: Get all sortie information for this mission
Check-Your-Fate->>Check-Your-Fate: Calculate fate
Check-Your-Fate -->> PAM: Write fates for mission
Note left of PAM: Not implemented yet
Check-Your-Fate ->> Map-Tool: Data to be shown
PAM -->> Map-Tool: Data to be shown
Note right of Map-Tool: Not implemented yet
```
## Dependencies
**Build with python 3.9**

#pip
install mysql-connector
install psycopg2

## Il2 stats db
'''

## PAM
'''

## Check-Your-Fate
'''

## Map-Tool
'''
