import json
import event_handler
import os


def populate_mission_data(log_data):
    """Creates a dictionary object containing data about the pilots, aircraft and sorties from this mission"""
    # Create empty dictionary to store pilot status information
    data = {
        'pilot_status': {},
        'mapping': {}
    }
    
    # Compile a list of unique pilot ids and names from the spawn events
    matches = event_handler.spawn_event.finditer(log_data)
    unique_pilots = set([(match.group('account_id'), match.group('name')) for match in matches])
    
    # Add each unique pilot to pilot_status
    for pilot in unique_pilots:
        data['pilot_status'][pilot[0]] = {
            'name': pilot[1],
            'sorties': {} }

    # Add a sortie to each pilot per spawn event
    matches = event_handler.spawn_event.finditer(log_data)
    for match in matches:
        account_id = match.group('account_id')
        pid = match.group('bot_id')
        aid = match.group('aircraft_id')
        
        # Create pilot data entry
        data['pilot_status'][account_id]['sorties'][aid] = {
            'aid': aid,
            'pid': pid,
            'started': 'unknown',
            'ended': 'unknown',
            'pilot_fate': 'unknown',
            'pilot_hp': 1.0,
            'aircraft': match.group('aircraft_name'),
            'aircraft_fate': 'unknown',
            'aircraft_hp': 1.0,
            'last_position': 'unknown',
            'events': [] }
        
        # Create mapping between aid and pid
        data['mapping'][pid] = {
            'type': 'pid',
            'aid': aid,
            'account': account_id
        }
        data['mapping'][aid] = {
            'type': 'aid',
            'pid': pid, 
            'account': account_id
        }

    return data

def read_log(filepath):
    """Read through a specified log file and return a Pilot Status dictionary containing pilot ids, names, sorties and relevant sortie events"""
    
    # load file
    log_lines = ""
    with open(filepath, 'r') as log_file:
        log_lines = log_file.read()

    # Populate pilots, sorties and aircraft list
    mission_data = populate_mission_data(log_lines)

    # Add events to sorties
    for line in log_lines.split('\n'):
        event = event_handler.parse_event(line)

        # Note takeoff and landing positions and times
        if event['type'] in ('takeoff', 'landing'):
            aid = event['aid']
            account = mission_data['mapping'][aid]['account']
            
            mission_data['pilot_status'][account]['sorties'][aid]['events'].append(event)

            if event['type'] == 'takeoff':
                mission_data['pilot_status'][account]['sorties'][aid]['started'] = event['tik']

        # Note sortie end/disconnection events for known players
        if event['type'] in ('disconnected', 'sortie_end') and event['pid'] in mission_data['mapping']:
            pid = event['pid']
            aid = mission_data['mapping'][pid]['aid']
            account = mission_data['mapping'][pid]['account']
            
            mission_data['pilot_status'][account]['sorties'][aid]['events'].append(event)
            
            if event['type'] == 'sortie_end':
                mission_data['pilot_status'][account]['sorties'][aid]['ended'] = event['tik']

        # Apply damage to pilot/aircraft for known players
        if event['type'] == 'damaged' and event['target_id'] in mission_data['mapping']:
            tid = event['target_id']
            
            # Pilot injury
            if mission_data['mapping'][tid]['type'] == 'pid':
                pid = tid
                aid = mission_data['mapping'][tid]['aid']
                account = mission_data['mapping'][tid]['account']
                mission_data['pilot_status'][account]['sorties'][aid]['pilot_hp'] -= event['damage']

            # Aircraft damage
            if mission_data['mapping'][tid]['type'] == 'aid':
                aid = tid
                pid = mission_data['mapping'][tid]['pid']
                account = mission_data['mapping'][tid]['account']
                mission_data['pilot_status'][account]['sorties'][aid]['aircraft_hp'] -= event['damage']

        # Note aircraft destruction/pilot death events for known players
        if event['type'] == 'destroyed' and event['target_id'] in mission_data['mapping']:
            tid = event['target_id']
            aid = None
            pid = None

            # Pilot death
            if mission_data['mapping'][tid]['type'] == 'pid':
                pid = tid
                aid = mission_data['mapping'][tid]['aid']
                event['type'] = 'killed'

            # Aircraft destruction
            if mission_data['mapping'][tid]['type'] == 'aid':
                aid = tid
                pid = mission_data['mapping'][tid]['pid']

            account = mission_data['mapping'][aid]['account']
            mission_data['pilot_status'][account]['sorties'][aid]['events'].append(event)

            if event['type'] == 'killed':
                mission_data['pilot_status'][account]['sorties'][aid]['pilot_fate'] = 'KIA'
                mission_data['pilot_status'][account]['sorties'][aid]['pilot_hp'] = 0.0

            if event['type'] == 'destroyed':
                mission_data['pilot_status'][account]['sorties'][aid]['aircraft_fate'] = 'Lost'
                mission_data['pilot_status'][account]['sorties'][aid]['aircraft_hp'] = 0.0
        
    return mission_data


# If called directly, run parser against specified test file and write output to json
if __name__=='__main__':
    mission_data = read_log('test logs\\missionReport(2021-11-07_14-01-21)[0].txt')
    if not os.path.exists('outputs/'):
        os.mkdir('outputs/')
    
    with open('outputs/output.json', 'w') as output_file:
        output_file.write(json.dumps(mission_data['pilot_status'], indent=4))

    with open('outputs/mapping.json', 'w') as mapping_file:
        mapping_file.write(json.dumps(mission_data['mapping'], indent=4))
